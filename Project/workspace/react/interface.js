import React from 'react';
import axios from 'axios';

export class AddQuote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.state.name = "";
        this.state.country = "";
        this.state.birth = 0;
        this.state.languageName = "";
        this.state.isDefault = false;
        this.state.authorId = 0;
        this.state.languageId = 0;
        this.state.description = "";
        this.state.year = 0;
    }

    // handleChangeProductName = (event) => {
    //     this.setState({
    //         productName: event.target.value
    //     })
    // }


    // handleChangeProductPrice = (event) => {
    //     this.setState({
    //         price: event.target.price
    //     })
    // }

    addItem = () => {
        let author = {
            name: this.state.name,
            country: this.state.country,
            birth: this.state.birth
        }
        axios.post('https://project2-alexioanas.c9users.io/addAuthor', author).then((res) => {
            if (res.state === 200) {
                this.props.itemAdd(author)
        }
        }).catch((err) => {
            console.log(err)
        })
        let language = {
            languageName: this.state.languageName,
            isDefault: this.state.isDefault
        }
        axios.post('https://project2-alexioanas.c9users.io/addLanguage', author).then((res) => {
            if (res.state === 200) {
                this.props.itemAdd(author)
        }
        }).catch((err) => {
            console.log(err)
        })
        let quote = {
            authorId: this.state.authorId,
            languageId: this.state.languageId,
            description: this.state.description,
            year: this.state.year
        }
        axios.post('https://project2-alexioanas.c9users.io/addQuote', quote).then((res) => {
            if (res.state === 200) {
                this.props.itemAdd(quote)
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    render() {
        return (
            <div>
                <h1>Add product</h1>
                <input type="text" placeholder="Name"
                    // onChange={this.handleChangeProductName}
                    />
                <input type="number"
                    // onChange={this.handleChangeProductPrice}
                    />
                <button onClick={this.addItem}>Add Product</button>
            </div>
            );
    }

}
