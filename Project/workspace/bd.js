
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

var express = require('express');
const cors = require("cors");


const app = express();
app.use(cors());

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
	next();
  });

app.use(bodyParser.json());



const sequelize = new Sequelize('c9', 'alexioanas', '', { //ce username aici?
   host: 'localhost',
   dialect: 'mysql'
});


sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


// Define the Authors Model
const Authors = sequelize.define('authors', {
   id: { 
       type: Sequelize.INTEGER, 
       primaryKey: true, 
       autoIncrement: true 
   },
   name: {
      type: Sequelize.STRING,
      allowNull: false
   }, 
   country: {
       type: Sequelize.STRING,
       allowNull: true
   },
   birth: {
       type: Sequelize.INTEGER,
       allowNull: false
   }
});

// Define the Languages Model
const Languages = sequelize.define('languages', {
   id: { 
       type: Sequelize.INTEGER, 
       primaryKey: true, 
       autoIncrement: true 
   },
   languageName: {
      type: Sequelize.STRING,
      allowNull: false
   },
  isDefault: {
      type: Sequelize.BOOLEAN,
      allowNull: false
  }
});

//Define the Quotes Model
const Quotes = sequelize.define('quotes', {
   id: {
       type: Sequelize.INTEGER,
       autoIncrement: true, 
       primaryKey: true
   }, 
   authorId : {
       type: Sequelize.INTEGER,
       allowNull: false
   },
   languageId: {
       type: Sequelize.INTEGER,
       allowNull: false
   }, 
   description: {
        type: Sequelize.STRING,
        allowNull: false
   },
   year : {
       type: Sequelize.INTEGER,
       allowNull: true
   }
});

Quotes.belongsTo(Authors, { foreignKey: 'authorId' });
Quotes.belongsTo(Languages, { foreignKey: 'languageId' });


// Define the Ratings Model
const Ratings = sequelize.define('ratings', {
   id: { 
       type: Sequelize.INTEGER, 
       primaryKey: true, 
       autoIncrement: true 
   },
   rating: {
      type: Sequelize.INTEGER,
      allowNull: false
   }, 
   quoteId: {
       type: Sequelize.INTEGER,
       allowNull: false,
   }
});


Ratings.belongsTo(Quotes, { foreignKey: 'quoteId' }); 



sequelize.sync({force: true}).then(()=>{     
    console.log('Databases create successfully')
})



//CRUD
//Get All Quotes 
app.get('/getAllQuotes', (req,res) =>{
    Quotes.findAll().then((quote) =>{
        res.status(200).send(quote);    
    });
});

//Create Quote
app.post('/addQuote', (req,res) => {
    Quotes.create({
        description: req.body.description,
        languageId: req.body.languageId,
        authorId: req.body.authorId,
        year: req.body.year
    }).then((quote) => {
       res.status(200).send(quote); 
    }, (err) =>{
      res.status(500).send(err);  
    });
})

// //Create Quote
// app.post('/addQuote', (req,res) => {
//     Quotes.create({
//         description: req.body.description,
//         languageId: req.body.languageId,
//         authorId: req.body.authorId,
//         year: req.body.year
//     }).then((quote) => {
//       res.status(200).send(quote); 
//     }, (err) =>{
//       res.status(500).send(err);  
//     });
// })

//Create Author
app.post('/addAuthor', (req,res) => {
    Authors.create({
        name: req.body.name,
        country: req.body.country,
        birth: req.body.birth
    }).then((author) => {
       res.status(200).send(author); 
    }, (err) =>{
      res.status(500).send(err);  
    });
})

//Get All Authors 
app.get('/getAllAuthors', (req,res) =>{
    Authors.findAll().then((author) =>{
        res.status(200).send(author);
    });
});


//Create Language
app.post('/addLanguage', (req,res) => {
    Languages.create({
        languageName: req.body.languageName,
        isDefault: req.body.isDefault,
    }).then((language) => {
       res.status(200).send(language); 
    }, (err) =>{
      res.status(500).send(err);  
    });
})

//Get All Languages
app.get('/getAllLanguages', (req,res) =>{
    Languages.findAll().then((language) =>{
        res.status(200).send(language);
    });
});

//Create Quote
app.post('/addQuote', (req,res) => {
    Quotes.create({
        authorId: req.body.authorId,
        languageId: req.body.languageId,
        description: req.body.description,
        year: req.body.year
    }).then((quote) => {
       res.status(200).send(quote); 
    }, (err) =>{
      res.status(500).send(err);  
    });
})

//Get All Quotes
app.get('/getAllQuotes', (req,res) =>{
    Quotes.findAll().then((quote) =>{
        res.status(200).send(quote);
    });
});

//Create Rating
app.post('/addRating', (req,res) => {
    Ratings.create({
        quoteId: req.body.quoteId,
        rating: req.body.rating,
    }).then((rating) => {
       res.status(200).send(rating); 
    }, (err) =>{
      res.status(500).send(err);  
    });
})

//Get All Ratings
app.get('/getAllRatings', (req,res) =>{
    Ratings.findAll().then((rating) =>{
        res.status(200).send(rating);
    });
});


//Delete Quote
app.delete('/deleteQuote/:id',(req,res)=>{
    Quotes.findById(req.params.id).then((quote)=>{
        if(quote){
            quote.destroy().then((result)=>{
                res.status(204).send();
            }).catch((err)=>{
                console.log(err);
                res.status(500).send('Error in database');
            })
        }
        else{
            res.status(404).send('Quote not found!');
        }
    }).catch((err)=>{
        console.lo(err);
        res.status(500).send('Error in database');
    });
});


//Update Quote
app.put('/updateQuote', (req, res) =>{
    const id = req.params.id;
    Quotes.findById(req.params.id).then((quote)=>{
        if(quote){
            quote.authorId = req.body.authorId,
            quote.languageId = req.body.languageId,
            quote.description = req.body.description,
            quote.year = req.body.year
            res.status(200).send(`Quote ${quote.id} updated!`);    
        }
        else{
            res.status(404).send(`Could not find quote with id ${quote.id}`);
        }
    });
});

app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
})
